# Mini S3 - Rails

[![build status](https://gitlab.com/phunehehe/mini-s3-rails/badges/master/build.svg)](https://gitlab.com/phunehehe/mini-s3-rails/pipelines)
[![coverage report](https://gitlab.com/phunehehe/mini-s3-rails/badges/master/coverage.svg)](https://phunehehe.gitlab.io/mini-s3-rails/coverage/)

This is a toy implementation of a simple storage service using the [Rails
framework](https://github.com/rails/rails).

Install dependencies and run tests:

```
$ ./test/test.sh
+ gem install bundler
Successfully installed bundler-1.15.1
1 gem installed
+ ./bin/bundle install
Using rake 12.0.0
...
Using rails 5.1.1
Bundle complete! 9 Gemfile dependencies, 53 gems now installed.
Use `bundle info [gemname]` to see where a bundled gem is installed.
+ ./bin/rails test
Running via Spring preloader in process 20711
Run options: --seed 32146

# Running:

.........

Finished in 0.323454s, 27.8246 runs/s, 68.0158 assertions/s.
9 runs, 22 assertions, 0 failures, 0 errors, 0 skips
Coverage report generated for MiniTest to /home/phunehehe/mini-s3-rails/coverage. 32 / 32 LOC (100.0%) covered.
```

Start the development server:

```
$ rails server
=> Booting Puma
=> Rails 5.1.1 application starting in development on http://localhost:3000
=> Run `rails server -h` for more startup options
Puma starting in single mode...
* Version 3.9.1 (ruby 2.3.4-p301), codename: Private Caller
* Min threads: 5, max threads: 5
* Environment: development
* Listening on tcp://0.0.0.0:3000
Use Ctrl-C to stop
```

Upload a file:

```
$ curl --verbose --request PUT --data content=test localhost:3000/files/my-file
* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 3000 (#0)
> PUT /files/my-file HTTP/1.1
> User-Agent: curl/7.35.0
> Host: localhost:3000
> Accept: */*
> Content-Length: 12
> Content-Type: application/x-www-form-urlencoded
>
* upload completely sent off: 12 out of 12 bytes
< HTTP/1.1 204 No Content
< Cache-Control: no-cache
< X-Request-Id: f51b7b16-f699-41f4-9b71-4dfd1633d53b
< X-Runtime: 0.005309
<
* Connection #0 to host localhost left intact
```

List all files:

```
$ curl --silent localhost:3000/files | jq
[
  {
    "id": 1,
    "name": "my-file",
    "file_content_id": 1,
    "created_at": "2017-06-26T13:33:19.973Z",
    "updated_at": "2017-06-26T13:33:19.973Z"
  }
]

```

Download a file:

```
curl --verbose localhost:3000/files/my-file
* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 3000 (#0)
> GET /files/my-file HTTP/1.1
> User-Agent: curl/7.35.0
> Host: localhost:3000
> Accept: */*
>
< HTTP/1.1 200 OK
< Content-Type: application/octet-stream
< Content-Disposition: attachment
< Content-Transfer-Encoding: binary
< Cache-Control: private
< ETag: W/"9f86d081884c7d659a2feaa0c55ad015"
< X-Request-Id: cfaca595-2843-407c-99f2-b688687ea182
< X-Runtime: 0.004227
< Transfer-Encoding: chunked
<
* Connection #0 to host localhost left intact
test
```

Delete a file:

```
$ curl --verbose --request DELETE localhost:3000/files/my-file
* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 3000 (#0)
> DELETE /files/my-file HTTP/1.1
> User-Agent: curl/7.35.0
> Host: localhost:3000
> Accept: */*
>
< HTTP/1.1 204 No Content
< Cache-Control: no-cache
< X-Request-Id: e3f15eec-9dea-47df-9061-f6a82b8c311a
< X-Runtime: 1.073185
<
* Connection #0 to host localhost left intact
```
