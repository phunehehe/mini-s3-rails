class FilesController < ApplicationController

  def destroy
    FileMetadata.find_by(name: params[:name]).destroy
  end

  def index
    render json: FileMetadata.all
  end

  def show
    send_data FileMetadata.find_by!(name: params[:name]).file_content.content
  end

  # XXX: this is not a real upsert
  def upsert
    FileMetadata.transaction do
      content = FileContent.find_or_create params.require :content
      metadata = FileMetadata.find_or_initialize_by name: params[:name]
      metadata.update file_content: content
    end
  end

end
