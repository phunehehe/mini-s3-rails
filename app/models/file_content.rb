class FileContent < ApplicationRecord

  def self.find_or_create(content)
    digest = Digest::SHA256.base64digest content
    FileContent.find_or_create_by digest: digest do |c|
      c.content = content
    end
  end

  def prune
    destroy unless FileMetadata.exists? file_content: self
  end
end
