class FileMetadata < ApplicationRecord

  belongs_to :file_content

  after_destroy do
    file_content.prune
  end

  # XXX: this monkey patching feels fragile
  # Callbacks cannot be used because we won't have access to the old content
  # anymore. Dirty is a plausible solution, but seems like an overkill for now.
  def update(attributes)
    old_content = file_content
    super
    return unless attributes.key?(:file_content) && old_content.respond_to?(:prune)
    old_content.prune
  end
end
