ActiveSupport::Inflector.inflections do |inflect|

  # We never use metadatum. We need "sets of metadata" instead. So let's
  # just invent this irregular new word.
  inflect.irregular 'metadata', 'metadata'
end
