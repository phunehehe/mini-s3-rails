Rails.application.routes.draw do
  put 'files/:name', to: 'files#upsert'
  resources :files, param: :name, only: [
    :destroy,
    :index,
    :show,
  ]
end
