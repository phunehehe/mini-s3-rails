class CreateMyFiles < ActiveRecord::Migration[5.1]
  def change
    create_table :my_files do |t|
      t.string :name
      t.binary :content

      t.timestamps
    end
  end
end
