class AddIndexToMyFiles < ActiveRecord::Migration[5.1]
  def change
    add_index :my_files, :name, unique: true
  end
end
