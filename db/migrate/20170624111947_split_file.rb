class SplitFile < ActiveRecord::Migration[5.1]

  # my_files cannot be modified in place due to incomplete support for ALTER
  # TABLE in SQLite, so we create new tables, migrate the data, then drop the
  # old table
  # https://sqlite.org/faq.html#q11

  class MyFile < ApplicationRecord
  end
  class FileContent < ApplicationRecord
  end
  class FileMetadata < ApplicationRecord
  end

  def change

    create_table :file_contents do |t|
      t.string :digest, index: { unique: true }, null: false
      t.binary :content, null: false
    end

    create_table :file_metadata do |t|
      t.string :name, index: { unique: true }, null: false
      t.references :file_content, null: false, foreign_key: true
      t.timestamps
    end

    reversible do |dir|

      dir.up do
        MyFile.find_each do |file|
          digest = Digest::SHA256.base64digest file.content
          content = FileContent.find_or_create_by digest: digest do |c|
            c.content = file.content
          end
          FileMetadata.create name: file.name, file_content_id: content.id
        end
      end

      dir.down do
        FileMetadata.find_each do |metadata|
          content = FileContent.find(metadata.file_content_id).content
          MyFile.create name: metadata.name, content: content
        end
      end
    end

    drop_table :my_files do |t|
      t.string :name, index: { unique: true }, null: false
      t.binary :content
      t.timestamps
    end
  end
end
