# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170624111947) do

  create_table "file_contents", force: :cascade do |t|
    t.string "digest", null: false
    t.binary "content", null: false
    t.index ["digest"], name: "index_file_contents_on_digest", unique: true
  end

  create_table "file_metadata", force: :cascade do |t|
    t.string "name", null: false
    t.integer "file_content_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["file_content_id"], name: "index_file_metadata_on_file_content_id"
    t.index ["name"], name: "index_file_metadata_on_name", unique: true
  end

end
