let pkgs = import <nixpkgs> {};
in pkgs.stdenv.mkDerivation {
  name = "mini-s3-rails";
  buildInputs = with pkgs; [
    ruby
    sqlite.dev
    zlib.dev
  ];
}
