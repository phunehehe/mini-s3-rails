require 'test_helper'

class FilesControllerTest < ActionDispatch::IntegrationTest

  test 'should delete file' do
    assert_difference 'FileMetadata.count', -1 do
      assert_difference 'FileContent.count', -1 do
        delete file_url name: FileMetadata.take.name
        assert_response :success
      end
    end
  end

  test 'should keep shared content' do
    new_name = 'test'
    put file_url name: new_name, params: {
      content: FileContent.first.content,
    }
    assert_difference 'FileMetadata.count', -1 do
      assert_no_difference 'FileContent.count' do
        delete file_url name: new_name
        assert_response :success
      end
    end
  end

  test 'should list files' do
    get files_url
    assert_response :success
    assert JSON.parse(@response.body).respond_to? 'length'
  end

  test 'should show file' do
    get file_url name: FileMetadata.take.name
    assert_response :success
    assert_equal 'application/octet-stream', @response.content_type
  end

  test 'should handle missing file in show' do
    assert_raises ActiveRecord::RecordNotFound do
      get file_url name: 'missing'
      assert_response :missing
    end
  end

  test 'should create file' do
    assert_difference 'FileMetadata.count' do
      assert_difference 'FileContent.count' do
        put file_url name: 'test', params: {
          content: 'test',
        }
        assert_response :success
      end
    end
  end

  test 'should handle missing content' do
    assert_raises ActionController::ParameterMissing do
      put file_url name: 'test'
      assert_response 400
    end
  end

  test 'should update file' do
    assert_no_difference 'FileMetadata.count' do
      assert_no_difference 'FileContent.count' do
        metadata = FileMetadata.take
        new_content = 'test'
        put file_url name: metadata.name, params: {
          content: new_content,
        }
        assert_response :success
        metadata.reload
        assert_equal metadata.file_content.content, new_content
      end
    end
  end

  test 'should reuse content' do
    assert_difference 'FileMetadata.count' do
      assert_no_difference 'FileContent.count' do
        put file_url name: 'test', params: {
          content: FileContent.first.content,
        }
        assert_response :success
      end
    end
  end
end
