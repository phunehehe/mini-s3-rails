#!/usr/bin/env bash
set -efuxo pipefail

PATH="$(ruby -rubygems -e 'puts Gem.user_dir')/bin:$PATH"
gem install bundler
bundle install

rails test
rubocop
